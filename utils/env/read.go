// © Akbarshoh Ismoilov [2024]

package env

import (
	"os"
)

type Context struct {
	RbHome   string
	RbConfig string
}

func Get() (ctx *Context) {
	return &Context{
		RbHome:   os.Getenv("RB_HOME"),
		RbConfig: os.Getenv("RB_CONFIG"),
	}
}
