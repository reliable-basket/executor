// © Akbarshoh Ismoilov [2024]

package env

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func init() {
	file, err := os.Open("rb.env")
	if err != nil {
		log.Fatalf("utils/env/read.go reading rb.env failed: %v", err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		parts := strings.SplitN(line, "=", 2)
		if len(parts) != 2 {
			continue
		}

		key := strings.TrimSpace(parts[0])

		value := os.ExpandEnv(strings.TrimSpace(parts[1]))

		err = os.Setenv(key, value)
		if err != nil {
			log.Fatalf("utils/env/read.go setting rb.env variables failed: %v", err)
		}
	}
}
