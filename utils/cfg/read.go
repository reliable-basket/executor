// © Akbarshoh Ismoilov [2024]

package cfg

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
	"reliable-basket/utils/env"
)

type Config struct {
	BDest string `yaml:"backup_dest" default:"assets/warehouse.db"`
}

func Read() (cfg *Config) {
	en := env.Get()
	yb, err := os.ReadFile(en.RbConfig)
	if err != nil {
		log.Fatalf("utils/cfg/read.go reading config file failed: %s", err)
	}

	err = yaml.Unmarshal(yb, &cfg)
	if err != nil {
		log.Fatalf("utils/cfg/read.go unmarshaling config.yaml into stuct failed: %s", err)
	}

	return
}
