// © Akbarshoh Ismoilov [2024]

// RB - reliable basket is NoSQL custom database
// creating for educational purposes

package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"reliable-basket/storage"
	"reliable-basket/utils/cfg"
	"reliable-basket/utils/env"
	"syscall"
)

func main() {
	warehouse := storage.New()

	go func() {
		shutdown(warehouse)
	}()

	//tcp.NewNetwork()

	fmt.Println("config:", cfg.Read())
	fmt.Println("env:", env.Get())
}

func shutdown(w *storage.Warehouse) {
	c := make(chan os.Signal)

	// registering c as os.Interrupt, syscall.SIGTERM
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		<-c

		// backup nodes before shutdown
		w.Write2File()

		log.Println("received shutdown signal")
		os.Exit(1)
	}()
}

// TODO: implement tests

// TODO: implement http-server [CRUD]
