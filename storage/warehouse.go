// © Akbarshoh Ismoilov [2024]

package storage

import (
	"fmt"
	"log"
	"os"
	"reliable-basket/utils/cfg"
	"reliable-basket/utils/env"
)

type Warehouse struct {
	First *Node

	// added to insert into Node's list seamlessly
	Last   *Node
	Length int

	// added to improve performance at retrieving Node which getter asks
	cache map[string]*Node

	// *os.File type for storing Nodes if Machine shuts down eventually
	snapshot *os.File
}

func New() *Warehouse {
	c := cfg.Read()

	f, err := os.OpenFile(fmt.Sprintf("%s/%s", env.Get().RbHome, c.BDest), os.O_RDWR, 0666)
	if err != nil {
		log.Fatal("storage/warehouse.go open backup file failed:", err)
	}
	// avoid panic, while adding first Node
	m := make(map[string]*Node)

	return &Warehouse{
		cache:    m,
		snapshot: f,
	}
}

// INTERNAL FUNCs

// insert : if there is no Node with the key, the func inserts new Node with it
func (w *Warehouse) insert(value []byte, key string) {
	if value == nil || key == "" {
		return
	}

	// if key already exists
	if _, ok := w.cache[key]; ok {
		r := w.cache[key]

		r.Value = value

		return
	}

	if w.cache == nil {
		w.cache = make(map[string]*Node)
	}
	n := newNode(value, key)

	// if there is no Nodes
	if w.Length == 0 {
		w.First = n
		w.Last = n

		w.Length = 1

		// add to cache
		w.cache[key] = n
		return
	}

	n.Prev = w.Last
	w.Last.Next = n

	w.Last = n

	w.Length++

	w.cache[key] = n
}
