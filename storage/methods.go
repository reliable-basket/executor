// © Akbarshoh Ismoilov [2024]

package storage

import (
	"fmt"
	"log"
	"os"
	cfg2 "reliable-basket/utils/cfg"
	"reliable-basket/utils/env"
)

// Insert : uses internal insert() func
func (w *Warehouse) Insert(value []byte, key string) {
	w.insert(value, key)
}

func (w *Warehouse) Get(key string) *Node {
	if w.First == nil {
		return nil
	}

	r := w.cache[key]
	return r
}

func (w *Warehouse) Delete(key string) {
	if w.First == nil {
		return
	}

	r := w.cache[key]

	if w.Length == 1 {
		w.Length = 0
		w.Last = nil
		w.First = nil

		delete(w.cache, key)
		return
	}

	if r.Key == w.Last.Key {
		w.Last = r.Prev
		r.Prev.Next = nil
	} else if r.Key == w.First.Key {
		w.First = r.Next
		r.Next.Prev = nil
	} else {
		r.Prev.Next = r.Next
	}

	w.Length--

	// delete permanently
	delete(w.cache, key)
}

// Update : uses internal insert() func
func (w *Warehouse) Update(value []byte, key string) {
	w.insert(value, key)
}

// Write2File : writes to file when os.Interrupt or syscall.SIGTERM occurs
func (w *Warehouse) Write2File() {
	c := cfg2.Read()

	if w.snapshot == nil {
		f, err := os.OpenFile(fmt.Sprintf("%s/%s", env.Get().RbHome, c.BDest), os.O_RDWR, 0666)
		if err != nil {
			log.Fatal("storage/methods.go accidental open backup failed:", err)
		}

		w.snapshot = f
	}

	cache := fmt.Sprint(w.cache)

	_, err := w.snapshot.Write([]byte(cache))
	if err != nil {
		log.Println("storage/methods.go writing to file failed:", err)
	}
}
