package storage

import (
	"fmt"
	"testing"
)

func TestWarehouse_Insert(t *testing.T) {
	fields := []struct {
		name  string
		input []byte
		key   string
		want  []byte
	}{
		{
			name:  "uz",
			input: []byte("{'uz': 'title'}"),
			key:   "uz",
			want:  []byte("{'uz': 'title'}"),
		},
		{
			name:  "duplicate",
			input: []byte("duplicated input"),
			key:   "uz",
			want:  []byte("duplicated input"),
		},
		{
			name:  "next value for delete method",
			input: []byte("next"),
			key:   "next",
			want:  []byte("next"),
		},
		{
			name:  "second value for delete method",
			input: []byte("second"),
			key:   "2",
			want:  []byte("second"),
		},
		{
			name:  "third value for delete method",
			input: []byte("third"),
			key:   "third",
			want:  []byte("third"),
		},
	}

	for _, field := range fields {
		t.Run(field.name, func(t *testing.T) {
			w.Insert(field.input, field.key)

			if string(w.Get(field.key).Value) != string(field.want) {
				t.Errorf("there is error: %s!=%s", field.input, field.want)
			}
		})
	}
}

func TestWarehouse_Update(t *testing.T) {
	fields := []struct {
		name  string
		input []byte
		key   string
		want  []byte
	}{
		{
			name:  "uz",
			input: []byte("updated version"),
			key:   "uz",
			want:  []byte("updated version"),
		},
	}

	for _, field := range fields {
		t.Run(field.name, func(t *testing.T) {
			w.Update(field.input, field.key)

			if string(w.Get(field.key).Value) != string(field.want) {
				t.Errorf("there is error: %s!=%s", field.input, field.want)
			}
		})
	}
}

func TestWarehouse_Delete(t *testing.T) {
	fields := []struct {
		name string
		key  string
	}{
		{
			name: "Delete uz key",
			key:  "uz",
		},
		{
			name: "Delete next value",
			key:  "next",
		},
		{
			name: "Delete third value",
			key:  "third",
		},
		{
			name: "Delete second value",
			key:  "second",
		},
	}

	for _, field := range fields {
		t.Run(field.name, func(t *testing.T) {
			w.Delete(field.key)

			if w.Get(field.key) != nil {
				t.Errorf("there is error: %v!=%v", w.Get(field.key), nil)
			}
		})
	}
}

func TestWarehouse_Write2File(t *testing.T) {
	w.Write2File()

	if w.snapshot == nil {
		fmt.Println("snapshot is nil:", w.snapshot)
	}
	stat, err := w.snapshot.Stat()
	if err != nil {
		t.Fatal("stat:", err)
	}

	if stat.Size() == 0 {
		t.Errorf("snapshot size should not be %d", stat.Size())
	}

	t.Log("size:", stat.Size())
}

func TestWarehouse_Write2File2(t *testing.T) {
	w.snapshot = nil

	w.Write2File()

	stat, err := w.snapshot.Stat()
	if err != nil {
		t.Fatal("stat:", err)
	}

	if stat.Size() == 0 {
		t.Errorf("snapshot size should not be %d", stat.Size())
	}

	t.Log("size:", stat.Size())
}
