package storage

import (
	"testing"
)

var (
	w *Warehouse = New()
)

func TestInsert(t *testing.T) {
	fields := []struct {
		name  string
		value []byte
		key   string
		want  []byte
	}{
		{
			name:  "every thing is good",
			value: []byte("test case for every thing is good"),
			key:   "good",
			want:  []byte("test case for every thing is good"),
		},
		{
			name:  "empty key",
			value: []byte("test case for empty key"),
		},
		{
			name:  "duplicate key",
			value: []byte("test case for duplicate key"),
			key:   "good",
			want:  []byte("test case for duplicate key"),
		},
		{
			name:  "second good test case",
			value: []byte("test case for second good test case"),
			key:   "second",
			want:  []byte("test case for second good test case"),
		},
	}

	for _, field := range fields {
		t.Run(field.name, func(t *testing.T) {
			w.insert(field.value, field.key)

			if field.name == "empty key" {
				if w.Get(field.key) != nil {
					t.Errorf("key %s should not be exist", field.key)
				}

				t.Skip()
			}

			if string(w.Get(field.key).Value) != string(field.want) {
				t.Errorf("there is error: want '%s', got '%s'", string(field.want), string(w.Get(field.key).Value))
			}
			if string(w.Last.Value) != string(field.want) {
				t.Errorf("w.Last should set to '%s', got '%s'", string(field.want), string(w.Last.Value))
			}
		})
	}
}
