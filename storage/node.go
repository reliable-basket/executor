// © Akbarshoh Ismoilov [2024]

package storage

type Node struct {
	Next  *Node
	Prev  *Node
	Value []byte
	Key   string
}

func newNode(value []byte, key string) *Node {
	var node = Node{
		Value: value,
		Key:   key,
	}

	return &node
}
