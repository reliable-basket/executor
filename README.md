## home path
```
~/.rb
```

## config file located at
```
~/.rb/config/config.yaml
```

## default backup file path
```
~/.rb/assets/warehouse.db
```

## to install rb
```bash
chmod +x install.sh

./install.sh
```

## default backup path
```
$RB_HOME/BPath/BFile

BPath and PFile located is config file
```