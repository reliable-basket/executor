package tcp

import (
	"fmt"
	"log"
	"net"
	"reliable-basket/storage"
)

func NewNetwork() {
	listener, err := net.Listen("tcp", ":6666")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		_ = listener.Close()
	}()
	fmt.Println("network launched at :6666")

	// for multiple connections, we'd use access keys for handling user's
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
		}

		wh := storage.New()

		// we do use 'public' key for default conn
		key := "public"

		h := NewHandler(key, conn, wh)
		go h.Handle()
	}
}
