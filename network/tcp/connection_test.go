package tcp

import (
	"io"
	"net"
	"testing"
)

func TestNewNetwork(t *testing.T) {
	conn, err := net.Dial("tcp", "127.0.0.1:6666")
	if err != nil {
		t.Error(err)
	}
	defer func() {
		_ = conn.Close()
	}()

	cases := []struct {
		key  string
		name string
		want string
	}{
		{
			key:  "ping",
			name: "ping as success",
			want: "pong",
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			_, err = conn.Write([]byte(c.key))
			if err != nil {
				t.Error(err)
			}

			res := make([]byte, 1024)
			n, err := conn.Read(res)
			if err != nil && err != io.EOF {
				t.Error(err)
			}
			t.Log("Response:", string(res))

			if string(res[:n]) != c.want {
				t.Errorf("got %s, want %s", string(res), c.want)
			}
		})
	}
}
