package tcp

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"reliable-basket/storage"
)

type Handler struct {
	// access key for authentication
	key string

	// conn for handling connection, receiving and sending data segments
	conn net.Conn

	// warehouse for storing data sets
	wh *storage.Warehouse
}

func NewHandler(key string, conn net.Conn, wh *storage.Warehouse) *Handler {
	return &Handler{key, conn, wh}
}

func (h *Handler) Handle() {
	for {
		var buffer bytes.Buffer
		tempBuf := make([]byte, 1)
		for {
			n, err := h.conn.Read(tempBuf)
			if err != nil {
				if err == io.EOF {
					break // End of data
				}
				log.Fatal("error while reading:", err)
			}
			buffer.Write(tempBuf[:n])
		}
		fmt.Println(buffer.String())

		_, _ = h.conn.Write([]byte("pong"))
	}
}
