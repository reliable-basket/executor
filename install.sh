#!/bin/bash

mkdir $HOME/.rb
if [ $? -eq 0 ]; then
    echo "\n- home directory set"
else
    echo "\n- home directory already exists"
fi

export RB_HOME=$HOME/.rb

mkdir -p $RB_HOME/assets && touch $RB_HOME/assets/warehouse.db
if [ $? -eq 0 ]; then
    echo "- warehouse file created for storing nodes when shutdown occurs"
else
    echo "- error creating assets and assets/warehouse.db"
fi

echo "\n- setup completed successfully"